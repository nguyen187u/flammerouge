public class Tuile {
    private int pente;
    private int nombreDeCases;
    private String nomTuile;
    private boolean[] caseoccupee;
    
    public Tuile(int pente, int nombreDeCases, String nomTuile) {
        this.pente = pente;
        this.nombreDeCases = nombreDeCases;
        this.nomTuile = nomTuile;
        this.caseoccupee = new boolean[nombreDeCases];
        for (int i = 0; i<nombreDeCases; i++) {
            caseoccupee[i] = false;
        }
    }

    public void setOccupe(Cycliste c) {
        this.caseoccupee[c.getPositionActuelle()] = true;
    }

    public int getNombreDeCases() {
        return nombreDeCases;
    }

    public boolean[] getCaseoccupee() {
        return caseoccupee;
    }

    public String getNomTuile() {
        return nomTuile;
    }

    public boolean estMontee() {
        boolean res = false;
        if (this.nomTuile.equals("VLM") || this.nomTuile.equals("VSM") || this.nomTuile.equals("LDM") || this.nomTuile.equals("LAM") || this.nomTuile.equals("LNM"))
            res = true;
        return res;
    }

    public boolean estDescente() {
        boolean res = false;
        if (this.nomTuile.equals("VLD") || this.nomTuile.equals("VSD") || this.nomTuile.equals("LDD") || this.nomTuile.equals("LAD") || this.nomTuile.equals("LND"))
            res = true;
        return res;
    }

    public boolean estLigneDroite() {
        return false;
    }
}
