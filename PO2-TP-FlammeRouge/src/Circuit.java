import java.util.ArrayList;
import java.util.Scanner;

public class Circuit {
	//Construire un Circuit de jeu
	private String nomCircuit;
	private int position;
	private ArrayList<Tuile> tuiles;
	//Constructeur parametre, inserer des tuiles
	public Circuit( String s) {
		this.setCircuit(s);
		this.position = 0;
		this.tuiles = new ArrayList<Tuile>(21);
		for (int i = 0; i <21; i++) {
			Scanner sc = new Scanner(System.in);
			int pente = sc.nextInt();
			int nombreDeCasses = sc.nextInt();
			String nomTuile = sc.next();
			this.tuiles.add(new Tuile(pente,nombreDeCasses,nomTuile));
		}
	}

	public ArrayList<Tuile> getTuiles() {
		return tuiles;
	}

	public void setCircuit(String s) {
		this.nomCircuit = s;
	}
	//retourne la tuile à la position a
	public Tuile tuileALaPosition(int a) {
		return this.tuiles.get(a);
	}
	//retourne true si c'est la fin de la liste de tuiles
	public boolean estALaDernierePosition() {
		boolean res = false;
		if (this.position==21) {
			res = true;
		}
		return res;
	}
	
	public void changerLaPosition() {
		this.position++;
	}

	public boolean positionEnDescente(int a) {
		boolean res = false;
		if(this.tuiles.get(a).estDescente()) {
			res = true;
		}
		return res;
	}

	public boolean positionEnVoieNormale(int a) {
		boolean res = false;
		if(!this.tuiles.get(a).estMontee() || !this.tuiles.get(a).estDescente()) {
			res = true;
		}
		return res;
	}

}
