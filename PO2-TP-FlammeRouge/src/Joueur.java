public class Joueur {
    private String nomJoueur;
    private String nomEquipe;
    private Rouleur rouleur;
    private Sprinter sprinter;
    //Créer un joueur avec un Rouleur et un Sprinteur
    public Joueur(String nomJoueur, String nomEquipe) {
        this.nomJoueur = nomJoueur;
        this.nomEquipe = nomEquipe;
        this.rouleur = new Rouleur();
        this.sprinter = new Sprinter();
    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public Rouleur getRouleur() {
        return rouleur;
    }

    public Sprinter getSprinter() {
        return sprinter;
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public void setRouleur(Rouleur rouleur) {
        this.rouleur = rouleur;
    }

    public void setSprinter(Sprinter sprinter) {
        this.sprinter = sprinter;
    }
}
