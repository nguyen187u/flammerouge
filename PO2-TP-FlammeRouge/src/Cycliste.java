import java.util.ArrayList;

public class Cycliste {
	//Créer une cycliste de jeu
	private int positionActuelle = 0;
	private boolean fileDroite = true;
	private Paquet paquet;
	private ArrayList<Carte> listeCartesDefausses;
	private ArrayList<Carte> listeCartesFaceCachee;
	private ArrayList<Carte> pioche;
	
	public Cycliste() {
		this.paquet = new Paquet();
		this.listeCartesDefausses = new ArrayList<Carte>();
		this.listeCartesFaceCachee = new ArrayList<Carte>();
		this.pioche = new ArrayList<Carte>();
	}


	public int getPositionActuelle() {
		return positionActuelle;
	}

	public void setPositionActuelle(int positionActuelle) {
		this.positionActuelle = positionActuelle;
	}

	public boolean isFileDroite() {
		return fileDroite;
	}

	public void setFileDroite(boolean fileDroite) {
		this.fileDroite = fileDroite;
	}
	//Piocher 4 cartes énergie, s'il n'y a pas assez de cartes, utiliser les cartes defausses
	public ArrayList<Carte> piocher4Cartes() {
		if(paquet.sizePaquet()<4) {
			paquet.addCarte(listeCartesDefausses);
		}
		for (int i = 0; i < 4; i++) {
			this.pioche.add(paquet.piocheCarte());
		}
		return this.pioche;
	}
	//Choisir une carte dans 4 cartes pioché
	public void choisirCarte(int a) {
		this.listeCartesFaceCachee.add(this.pioche.get(a));
		this.pioche.remove(a);
		this.listeCartesDefausses.addAll(pioche);
		this.pioche.clear();
	}
	// faire avancer des cyclistes
	public void avancer() {
		Carte ca = this.listeCartesFaceCachee.get(0);
		this.setPositionActuelle(this.getPositionActuelle()+ca.getNumero());
		this.listeCartesFaceCachee.remove(0);
	}


	@Override
	public String toString() {
		return " position actuelle = " + positionActuelle + "\n";
	}
}

