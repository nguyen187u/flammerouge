import java.util.ArrayList;
import java.util.Collection;

public class Paquet {
    private ArrayList<Carte> cartes;
    //Paquet de carte énergie
    public Paquet() {
        this.cartes = new ArrayList<Carte>(15);
        for (int i = 0; i<15; i++) {
            this.cartes.add(new Carte());
        }
    }
    //piocher une carte en tête de la paque
    public Carte piocheCarte() {
        int last = this.cartes.lastIndexOf(cartes);
        Carte c = this.cartes.get(last);
        this.cartes.remove(last);
        return c;
    }

    public void addCarte(ArrayList<Carte> cd) {
        this.cartes.addAll(cd);
    }

    public int sizePaquet() {
        return this.cartes.size();
    }
}
