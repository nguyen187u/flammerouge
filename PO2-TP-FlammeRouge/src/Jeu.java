import java.util.ArrayList;
import java.util.Scanner;

public class Jeu {
    private String phaseDeJeu;
    private Circuit circuit;
    private ArrayList<Joueur> listeJouer;
    //Créer un jeu avec un nombre de joueur
    public Jeu(String phaseDeJeu, Circuit circuit, ArrayList<Joueur> listeJouer) {
        this.phaseDeJeu = phaseDeJeu;
        this.circuit = circuit;
        this.listeJouer = listeJouer;
    }

    public String getPhaseDeJeu() {
        return phaseDeJeu;
    }

    public Circuit getCircuit() {
        return circuit;
    }

    public boolean estFini() {
        boolean res = false;
        if(this.circuit.estALaDernierePosition() && this.circuit.getTuiles().get(20).getCaseoccupee()[this.circuit.getTuiles().get(20).getNombreDeCases()-1]) {
            res = true;
        }
        return res;
    }

    public void demarrerJeu() {
        while (!this.estFini()) {
            for (int i = 0; i<listeJouer.size(); i++) {
                Joueur a = this.listeJouer.get(i);
                a.getRouleur().piocher4Cartes();
                Scanner sc = new Scanner(System.in);
                int b = sc.nextInt();
                a.getRouleur().choisirCarte(b);
                a.getRouleur().avancer();
                a.getSprinter().piocher4Cartes();
                int c = sc.nextInt();
                a.getSprinter().choisirCarte(c);
                a.getSprinter().avancer();
            }
        }
    }
    
}
