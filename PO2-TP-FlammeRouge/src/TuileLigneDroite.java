public class TuileLigneDroite extends Tuile{


    public TuileLigneDroite(int pente, int nombreDeCases, String nomTuile) {
        super(pente, nombreDeCases, nomTuile);
    }
    public boolean estLigneDroite(){
        return true;
    }
    public boolean estLigneDepart() {
        boolean res = false;
        if (this.getNomTuile().equals("LDD") || this.getNomTuile().equals("LDM")) {
            res = true;
        }
        return res;
    }
    public boolean estLigneArrivee() {
        boolean res = false;
        if (this.getNomTuile().equals("LAD") || this.getNomTuile().equals("LAM")) {
            res = true;
        }
        return res;
    }
    public boolean estLigneNormale() {
        boolean res = false;
        if (this.getNomTuile().equals("LND") || this.getNomTuile().equals("LNM")) {
            res = true;
        }
        return res;
    }
}
