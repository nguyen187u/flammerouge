public class TuileVirage extends Tuile{
    public TuileVirage(int pente, int nombreDeCases, String nomTuile) {
        super(pente, nombreDeCases, nomTuile);
    }
    public boolean estLeger() {
        boolean res = false;
        if (this.getNomTuile().equals("VLD") || this.getNomTuile().equals("VLM")) {
            res = true;
        }
        return res;
    }
    public boolean estSerre() {
        boolean res = false;
        if (this.getNomTuile().equals("VSD") || this.getNomTuile().equals("VSM")) {
            res = true;
        }
        return res;
    }
    public boolean estLigneDroite() {
        return false;
    }

}
